﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper;
using System;
using System.Text;
using System.Threading;

namespace Pinger
{
    class Program
    {
        public static Producer producer;
        public static Consumer consumer;
        public static int Count = 1000;
        static void Main(string[] args)
        {
            Console.WriteLine("Pinger started!");
            producer = new Producer(RabbitConfig.HostName, "Ping-PongExchange", "pong_queue", "Pongkey");
            consumer = new Consumer(RabbitConfig.HostName, "Ping-PongExchange", "ping_queue", "Pingkey");
            consumer.ListenQueue += PrintMessage;
            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
        public static void PrintMessage(string mess)
        {
            Thread.Sleep(2500);
            Console.WriteLine($"Recived: {mess} processed in a - {DateTime.Now.ToString("HH:mm:ss")}");
            SendMessage();
        }
        public static void SendMessage()
        {
            producer.SendMessageToQueue($"{Count} Ping - {DateTime.Now.ToString("HH:mm:ss")}") ;
            Count++;
        }
    }
}
