﻿using RabbitMQ.Wrapper;
using System;
using System.Threading;

namespace Ponger
{
    class Program
    {
        public static Consumer consumer;
        public static Producer producer;
        public static int Count = 2000;
        static void Main(string[] args)
        {
            Console.WriteLine("Consumer is started!");
            producer = new Producer(RabbitConfig.HostName, "Ping-PongExchange", "ping_queue", "Pingkey");
            consumer = new Consumer(RabbitConfig.HostName, "Ping-PongExchange", "pong_queue", "Pongkey");
            consumer.ListenQueue += PrintMessage;
            SendMessage();
            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
        public static void PrintMessage(string mess)
        {
            Thread.Sleep(2500);
            Console.WriteLine($"Recived: {mess} processed in a - {DateTime.Now.ToString("HH:mm:ss")}");
            SendMessage();
        }
        public static void SendMessage()
        {
            producer.SendMessageToQueue($"{Count} Pong - {DateTime.Now.ToString("HH:mm:ss")}");
            Count++;
        }
    }
}
