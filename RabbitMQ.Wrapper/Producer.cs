﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper
{
    public class Producer : RabbitMqService, IDisposable
    {
        public Producer(string hostName, string exchange, string queue, string routingKey)
            : base(hostName, exchange, queue, routingKey)
        {

        }

        public void SendMessageToQueue(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(exchange: Exchange,
                                 routingKey: RoutingKey,
                                 basicProperties: null,
                                 body: body);
        }
    }
}
