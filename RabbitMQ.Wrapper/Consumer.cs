﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper
{
    public class Consumer : RabbitMqService
    {
        public EventingBasicConsumer consumer { get; private set; }
        public Action<string> ListenQueue;
        public Consumer(string hostName, string exchange, string queue, string routingKey)
           : base(hostName, exchange, queue, routingKey)
        {
            channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange, ExchangeType.Direct);

            channel.QueueDeclare(queue: Queue,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
            channel.QueueBind(Queue, Exchange, RoutingKey);
            consumer = new(channel);
            consumer.Received += Recived;
            channel.BasicConsume(queue: Queue,
                                    autoAck: false,
                                    consumer: consumer);

        }
        private void Recived(object sender, BasicDeliverEventArgs args)
        {
            var isOk = false;
            try
            {
                var body = args.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                ListenQueue?.Invoke(message);
                isOk = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (isOk)
                {
                    channel.BasicAck(args.DeliveryTag, isOk);
                }
            }
        }
    }
}
