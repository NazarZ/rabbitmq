﻿using RabbitMQ.Client;
using System;

namespace RabbitMQ.Wrapper
{
    public abstract class RabbitMqService : IDisposable
    {
        protected readonly ConnectionFactory factory;
        protected readonly IConnection connection;
        protected IModel channel;
        protected readonly string Exchange;
        protected readonly string RoutingKey;
        protected readonly string Queue;

        public RabbitMqService(string hostName, string exchange, string queue, string routingKey)
        {
            Exchange = exchange;
            Queue = queue;
            RoutingKey = routingKey;

            factory = new ConnectionFactory() { HostName = hostName };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange, ExchangeType.Direct);

            channel.QueueDeclare(queue: Queue,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
            channel.QueueBind(Queue, Exchange, RoutingKey);

        }

        public void Dispose()
        {
            connection.Dispose();
            channel.Dispose();
        }
    }
}
